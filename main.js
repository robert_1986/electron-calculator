"use strict";

var app = require("app");
var browserWindow = require("browser-window");

var mainWindow = null;

app.on("ready", function() {
    mainWindow = new browserWindow({
        frame: false,
        height: 654,
        width: 403,
        resizable: false
    });

    mainWindow.loadURL("file://" + __dirname + "/app/views/main.html");
});
