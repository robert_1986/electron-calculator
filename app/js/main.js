var currentInput = 0;

var nums = document.querySelectorAll('.button-number');
for(var i = 0; i < nums.length; i++ ) {
    nums[i].addEventListener('click', function(e) {
        var val = this.innerHTML;
        console.log('clicked: ' + val);
        updateCurrentInput(val);
    });
}

function updateCurrentInput(num) {

    if (currentInput == '0' ) {
        currentInput = num;
    } else {
        currentInput = currentInput + num;
    }
    document.querySelector('.main').innerHTML = currentInput;
    console.log(currentInput);
}

document.querySelector('.button-operator-clear').addEventListener('click', function(e) {
    document.querySelector('.main').innerHTML = '0';
    currentInput = 0;
});
